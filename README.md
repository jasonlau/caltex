###### Space Launch App
![space launch](docs/space_launch_2.gif "Recent")

###### Install App
Because product flavors are being used, the build variant will need to be selected. You can either select the build variant in Android Studio before installing, or run:

```
./gradlew clean installProdDebug
```
The "mock" flavour will run against a service that hosts a locally defined json
###### Assumptions/Notes
- There's no fancy error handling, just returning a Toast to show an error message for any service errors for now.
- It was not clear which field was the "thumbnail image". I took this as "image_url" in each object in the results array. All of these entries seem to be null. I have added an entry to the first result of the mock flavour json to preview what the thumbnail would look like.
- For the "of particular interest" requirement, I used a "warning" image in the main list to denote it. ![warning](docs/warning.png "Recent")
- The larger image of the rocket I took as the image_url in details of the rocket url (eg. https://spacelaunchnow.me/api/3.3.0/config/launcher/49/?format=json)
 
###### Functional tests (Espresso)
To run functional tests, you will need to use the "mock" flavor. This is so that the service can reliably return a result and the same result every time.

```
./gradlew clean connectedMockDebugAndroidTest
```
###### Third-Party / Open Source libraries
- Retrofit - HTTP client that turns HTTP API in to an interface
- retrofit2-kotlin-coroutines-adapter - allows Retrofit to return a coroutines future (Deferred)
- Koin - dependency injection library
- Coroutines - allows for writing asynchronous, non-blocking code
- Picasso - image loading library

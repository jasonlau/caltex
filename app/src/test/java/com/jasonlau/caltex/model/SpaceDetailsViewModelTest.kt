package com.jasonlau.caltex.model

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.jasonlau.caltex.CoroutineTestRule
import com.jasonlau.caltex.service.ServiceResult
import com.jasonlau.caltex.service.SpaceLaunchRepository
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.CoreMatchers.equalTo
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.RuleChain
import org.mockito.Mock
import org.mockito.MockitoAnnotations

@ExperimentalCoroutinesApi
class SpaceDetailsViewModelTest {
    private val coroutinesTestRule = CoroutineTestRule()

    @get:Rule
    var chain: RuleChain = RuleChain.outerRule(coroutinesTestRule)
        .around(InstantTaskExecutorRule())

    @Mock
    lateinit var spaceLaunchRepository: SpaceLaunchRepository

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun whenSpaceDetailsSuccessfulThenPopulateLiveData() = coroutinesTestRule.testDispatcher.runBlockingTest {
        val url = "http://image_url"
        val rocketDetails = RocketDetails("http://rocket_image_url", "description")
        whenever(spaceLaunchRepository.getRocketDetails(url)).thenReturn(ServiceResult.Success(rocketDetails))
        val spaceDetailsViewModel = SpaceDetailsViewModel(spaceLaunchRepository, coroutinesTestRule.testDispatcherProvider)
        spaceDetailsViewModel.getRocketDetails(url)
        assertThat(spaceDetailsViewModel.rocketDetails.value, equalTo(rocketDetails))
    }

    @Test
    fun whenSpaceDetailsErrorsThenPopulateErrorLiveData() = coroutinesTestRule.testDispatcher.runBlockingTest {
        val url = "http://image_url"
        val expectedErrorMessage = "some error"
        whenever(spaceLaunchRepository.getRocketDetails(url)).thenReturn(ServiceResult.Error(RuntimeException(expectedErrorMessage)))
        val spaceDetailsViewModel = SpaceDetailsViewModel(spaceLaunchRepository, coroutinesTestRule.testDispatcherProvider)
        spaceDetailsViewModel.getRocketDetails(url)
        assertThat(spaceDetailsViewModel.showError.value, equalTo(expectedErrorMessage))
    }

    @Test
    fun whenRocketImageUrlIsNullThenPopulateErrorLiveData() = coroutinesTestRule.testDispatcher.runBlockingTest {
        val url: String? = null
        val spaceDetailsViewModel = SpaceDetailsViewModel(spaceLaunchRepository, coroutinesTestRule.testDispatcherProvider)
        spaceDetailsViewModel.getRocketDetails(url)
        assertThat(spaceDetailsViewModel.showError.value, equalTo("Cannot find rocket image path"))
    }
}

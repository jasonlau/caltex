package com.jasonlau.caltex.model

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.jasonlau.caltex.CoroutineTestRule
import com.jasonlau.caltex.service.ServiceResult
import com.jasonlau.caltex.service.SpaceLaunchRepository
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.CoreMatchers.equalTo
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.RuleChain
import org.mockito.Mock
import org.mockito.MockitoAnnotations

@ExperimentalCoroutinesApi
class SpaceLaunchViewModelTest {
    private val coroutinesTestRule = CoroutineTestRule()

    @get:Rule
    var chain: RuleChain = RuleChain.outerRule(coroutinesTestRule)
        .around(InstantTaskExecutorRule())

    @Mock
    lateinit var spaceLaunchRepository: SpaceLaunchRepository

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun whenSpaceLaunchSuccessfulThenPopulateLiveData() = coroutinesTestRule.testDispatcher.runBlockingTest {
        val expectedResponse = SpaceLaunch(listOf())
        whenever(spaceLaunchRepository.getSpaceLaunches()).thenReturn(ServiceResult.Success(expectedResponse))
        val spaceLaunchViewModel = SpaceLaunchViewModel(spaceLaunchRepository, coroutinesTestRule.testDispatcherProvider)
        spaceLaunchViewModel.getSpaceLaunches()
        assertThat(spaceLaunchViewModel.spaceLaunch.value, equalTo(expectedResponse))
    }

    @Test
    fun whenSpaceLaunchErrorsThenPopulateErrorLiveData() = coroutinesTestRule.testDispatcher.runBlockingTest {
        val expectedErrorMessage = "some error"
        whenever(spaceLaunchRepository.getSpaceLaunches()).thenReturn(ServiceResult.Error(RuntimeException(expectedErrorMessage)))
        val spaceLaunchViewModel = SpaceLaunchViewModel(spaceLaunchRepository, coroutinesTestRule.testDispatcherProvider)
        spaceLaunchViewModel.getSpaceLaunches()
        assertThat(spaceLaunchViewModel.showError.value, equalTo(expectedErrorMessage))
    }
}

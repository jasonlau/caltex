package com.jasonlau.caltex.service

import com.jasonlau.caltex.model.SpaceLaunch
import com.google.gson.Gson
import com.jasonlau.caltex.model.RocketDetails

class SpaceLaunchRepositoryImpl(private val spaceLaunchService: SpaceLaunchService) :
    SpaceLaunchRepository {
    private val gson = Gson()

    override suspend fun getRocketDetails(url: String): ServiceResult<RocketDetails> =
        ServiceResult.Success(RocketDetails("https://spacelaunchnow-prod-east.nyc3.cdn.digitaloceanspaces.com/media/launcher_images/soyuz2520sta_image_20191210133244.jpg",
            "The Atlas V N22 is a crew rated variant of the Atlas V 422 - it sports two solid motor strap on boosters with two liquid fueled stages. " +
                    "It will be primarly used to support NASA's Commercial Crew program by taking the CST-100 to orbit."))

    override suspend fun getSpaceLaunches(): ServiceResult<SpaceLaunch> =
        ServiceResult.Success(getTestSpaceLaunch())

    private fun getTestSpaceLaunch() : SpaceLaunch {
        val json = """
        {
          "count": 208,
          "next": "https://spacelaunchnow.me/api/3.3.0/launch/upcoming/?format=json&limit=20&offset=20",
          "previous": null,
          "results": [
            {
              "id": "556fe0d2-567a-4074-a20e-ec1809a6277f",
              "url": "https://spacelaunchnow.me/api/3.3.0/launch/556fe0d2-567a-4074-a20e-ec1809a6277f/?format=json",
              "launch_library_id": 1315,
              "slug": "https://spacelaunchnow.me/launch/soyuz-stafregat-csg-1-cheops-others",
              "name": "Soyuz STA/Fregat | CSG-1, CHEOPS, & others",
              "img_url": null,
              "status": {
                "id": 1,
                "name": "Go"
              },
              "net": "2019-12-18T08:54:20Z",
              "window_end": "2019-12-18T08:54:20Z",
              "window_start": "2019-12-18T08:54:20Z",
              "inhold": false,
              "tbdtime": false,
              "tbddate": false,
              "probability": -1,
              "holdreason": null,
              "failreason": null,
              "hashtag": null,
              "rocket": {
                "id": 47,
                "configuration": {
                  "id": 49,
                  "launch_library_id": 76,
                  "url": "https://spacelaunchnow.me/api/3.3.0/config/launcher/49/?format=json",
                  "name": "Soyuz STA",
                  "launch_service_provider": "Arianespace"
                },
                "launcher_stage": [],
                "spacecraft_stage": null
              },
              "mission": {
                "id": 828,
                "launch_library_id": 890,
                "name": "CSG-1, CHEOPS, & others",
                "description": "The COSMO-SkyMed Second Generation satellite (CSG-1) is an Earth observation spacecraft featuring state-of-the-art technologies and engineering solutions, further bolstering Italian leadership in this sector. It's one out of two satellites to replace the first generation COSMO-SkyMed system.\n\nThis second-generation system, including its ground segment, will set a new performance standard for space-based radar observation systems in terms of precision, image quality and the flexibility of user services. It is a dual (civil/military) system, designed to address the requirements of both commercial and government customers, as well as the scientific community. The COSMO-SkyMed Second Generation satellites are equipped with synthetic aperture radar (SAR), capable of observations under any weather or light conditions, day or night. \n\nThe Characterising Exoplanet Satellite (CHEOPS) is an ESA mission implemented in partnership – in particular with Switzerland.\n\nCHEOPS is the first mission dedicated to studying bright, nearby stars that already are known to host  exoplanets in order to make high-precision observations of the planet's size as it passes in front of its host star. The spacecraft will focus on planets in the super-Earth to Neptune size range, with its data enabling the bulk density of the planets to be derived – a first characterization step towards understanding these alien worlds.\n\nFlight also includes 3 auxiliary payloads: OPS-SAT, EyeSat, ANGELS.\n\nOPS-SAT is the world’s first free-for-use, in-orbit test bed for new software, applications and techniques in satellite control. It enables new software to be tested in orbit. In the first year of operation, OPS-SAT will host over 100 in-flight experiments submitted from many ESA Member States.\n\nEyeSat is a 3U CubeSat designed to study the zodiacal light and image the Milky Way. It's fitted with an instrument called IRIS, which is a small space telescope.\n\nANGELS is a 12U CubeSat, and is the French industry’s first nanosatellite. The satellite will be fitted with a miniaturized ARGOS Néo instrument, which is 10-times smaller than the equivalent previous-generation device. The instrument collects and determines the position of low-power signals and messages sent by the 20,000 ARGOS beacons now in service worldwide.",
                "type": "Dedicated Rideshare",
                "orbit": "Sun-Synchronous Orbit",
                "orbit_abbrev": "SSO"
              },
              "pad": {
                "id": 81,
                "agency_id": 115,
                "name": "Soyuz Launch Complex",
                "info_url": null,
                "wiki_url": "https://en.wikipedia.org/wiki/Ensemble_de_Lancement_Soyouz",
                "map_url": "https://www.google.com/maps/?q=5.3019,-52.8346",
                "latitude": "5.3019",
                "longitude": "-52.8346",
                "location": {
                  "id": 13,
                  "name": "Kourou, French Guiana",
                  "country_code": "GUF"
                }
              },
              "image_url": "https://spacelaunchnow-prod-east.nyc3.cdn.digitaloceanspaces.com/media/launcher_images/soyuz2520sta_image_20191210133244.jpg",
              "infographic_url": null
            },
            {
              "id": "c34d6078-6fcb-4d9d-a128-c116eeae4258",
              "url": "https://spacelaunchnow.me/api/3.3.0/launch/c34d6078-6fcb-4d9d-a128-c116eeae4258/?format=json",
              "launch_library_id": 1137,
              "slug": "https://spacelaunchnow.me/launch/long-march-4b-cbers-4a",
              "name": "Long March 4B | CBERS-4A",
              "img_url": null,
              "status": {
                "id": 1,
                "name": "Go"
              },
              "net": "2019-12-20T03:21:00Z",
              "window_end": "2019-12-20T03:41:00Z",
              "window_start": "2019-12-20T03:13:00Z",
              "inhold": false,
              "tbdtime": false,
              "tbddate": false,
              "probability": -1,
              "holdreason": null,
              "failreason": null,
              "hashtag": null,
              "rocket": {
                "id": 32,
                "configuration": {
                  "id": 10,
                  "launch_library_id": 16,
                  "url": "https://spacelaunchnow.me/api/3.3.0/config/launcher/10/?format=json",
                  "name": "Long March 4",
                  "launch_service_provider": "China Aerospace Science and Technology Corporation"
                },
                "launcher_stage": [],
                "spacecraft_stage": null
              },
              "mission": {
                "id": 1009,
                "launch_library_id": 1218,
                "name": "CBERS-4A",
                "description": "On April 24, 2015, China and Brazil have signed the agreement for the construction of CBERS-4A. It's a third satellite in the continuity to the CBERS program, and a sixth CBERS satellite to be constructed. The CBERS satellites enhance and complement the existing remote sensing systems in an effort to improve our knowledge about the Earth environment and resources.",
                "type": "Earth Science",
                "orbit": null
              },
              "pad": {
                "id": 25,
                "agency_id": null,
                "name": "Launch Complex 9",
                "info_url": null,
                "wiki_url": "",
                "map_url": "http://maps.google.com/maps?q=38.849+N,+111.608+E",
                "latitude": "38.849",
                "longitude": "111.608",
                "location": {
                  "id": 19,
                  "name": "Taiyuan, People's Republic of China",
                  "country_code": "CHN"
                }
              },
              "image_url": null,
              "infographic_url": null
            },
            {
              "id": "d6449a4e-7059-4623-8b64-7e7a8a6985a5",
              "url": "https://spacelaunchnow.me/api/3.3.0/launch/d6449a4e-7059-4623-8b64-7e7a8a6985a5/?format=json",
              "launch_library_id": 1355,
              "slug": "https://spacelaunchnow.me/launch/atlas-v-n22-cst-100-starliner-orbital-flight-test",
              "name": "Atlas V N22 | CST-100 Starliner Orbital Flight Test (Uncrewed)",
              "img_url": null,
              "status": {
                "id": 1,
                "name": "Go"
              },
              "net": "2019-12-20T11:36:00Z",
              "window_end": "2019-12-20T11:36:00Z",
              "window_start": "2019-12-20T11:36:00Z",
              "inhold": false,
              "tbdtime": false,
              "tbddate": false,
              "probability": 80,
              "holdreason": null,
              "failreason": null,
              "hashtag": null,
              "rocket": {
                "id": 1375,
                "configuration": {
                  "id": 166,
                  "launch_library_id": 118,
                  "url": "https://spacelaunchnow.me/api/3.3.0/config/launcher/166/?format=json",
                  "name": "Atlas V N22",
                  "launch_service_provider": "United Launch Alliance"
                },
                "launcher_stage": [],
                "spacecraft_stage": {
                  "id": 414,
                  "url": "https://spacelaunchnow.me/api/3.3.0/spacecraft/flight/414/?format=json",
                  "destination": "International Space Station",
                  "mission_end": null,
                  "spacecraft": {
                    "id": 288,
                    "url": "https://spacelaunchnow.me/api/3.3.0/spacecraft/288/?format=json",
                    "name": "Starliner Capsule 1",
                    "serial_number": null,
                    "status": {
                      "id": 1,
                      "name": "Active"
                    },
                    "description": "The first CST-100 Starliner capsule to fly. Will be flown on the first uncrewed orbital flight test mission.",
                    "configuration": {
                      "id": 9,
                      "url": "https://spacelaunchnow.me/api/3.3.0/config/spacecraft/9/?format=json",
                      "name": "CST-100 Starliner",
                      "type": {
                        "id": 1,
                        "name": "Unknown"
                      },
                      "agency": {
                        "id": 80,
                        "url": "https://spacelaunchnow.me/api/3.3.0/agencies/80/?format=json",
                        "name": "Boeing",
                        "type": "Commercial"
                      },
                      "in_use": true,
                      "image_url": "https://spacelaunchnow-prod-east.nyc3.cdn.digitaloceanspaces.com/media/orbiter_images/cst-1002520starliner_image_20190207032512.jpeg"
                    }
                  }
                }
              },
              "mission": {
                "id": 1050,
                "launch_library_id": 1249,
                "name": "CST-100 Starliner Orbital Flight Test (Uncrewed)",
                "description": "This is the first test flight of Starliner spacecraft to the International Space Station.",
                "type": "Test Flight",
                "orbit": "Low Earth Orbit",
                "orbit_abbrev": "LEO"
              },
              "pad": {
                "id": 29,
                "agency_id": null,
                "name": "Space Launch Complex 41",
                "info_url": null,
                "wiki_url": "https://en.wikipedia.org/wiki/Cape_Canaveral_Air_Force_Station_Space_Launch_Complex_41",
                "map_url": "http://maps.google.com/maps?q=28.58341025,-80.58303644",
                "latitude": "28.58341025",
                "longitude": "-80.58303644",
                "location": {
                  "id": 12,
                  "name": "Cape Canaveral, FL, USA",
                  "country_code": "USA"
                }
              },
              "image_url": null,
              "infographic_url": "https://spacelaunchnow-prod-east.nyc3.cdn.digitaloceanspaces.com/media/launch_images/atlas2520v252_infographic_20191206174658.png"
            },
            {
              "id": "1f28dc59-6f96-4c67-91a8-8ad860f72e86",
              "url": "https://spacelaunchnow.me/api/3.3.0/launch/1f28dc59-6f96-4c67-91a8-8ad860f72e86/?format=json",
              "launch_library_id": 1212,
              "slug": "https://spacelaunchnow.me/launch/proton-mblok-dm-03-elektro-l-no3",
              "name": "Proton-M/Blok DM-03 | Elektro-L No.3",
              "img_url": null,
              "status": {
                "id": 1,
                "name": "Go"
              },
              "net": "2019-12-24T12:20:00Z",
              "window_end": "2019-12-24T12:40:00Z",
              "window_start": "2019-12-24T12:00:00Z",
              "inhold": false,
              "tbdtime": false,
              "tbddate": false,
              "probability": -1,
              "holdreason": null,
              "failreason": null,
              "hashtag": null,
              "rocket": {
                "id": 2507,
                "configuration": {
                  "id": 46,
                  "launch_library_id": 62,
                  "url": "https://spacelaunchnow.me/api/3.3.0/config/launcher/46/?format=json",
                  "name": "Proton-M",
                  "launch_service_provider": "Khrunichev State Research and Production Space Center"
                },
                "launcher_stage": [],
                "spacecraft_stage": null
              },
              "mission": {
                "id": 673,
                "launch_library_id": 866,
                "name": "Elektro-L No.3",
                "description": "Elektro-L is a series of meteorological satellites developed for the Russian Federal Space Agency by NPO Lavochkin. They are designed to capture real-time images of clouds and the Earth’s underlying surface, heliogeophysical measurements, collection and translating hydrometeorological and service data.",
                "type": "Earth Science",
                "orbit": "Geostationary Transfer Orbit",
                "orbit_abbrev": "GTO"
              },
              "pad": {
                "id": 7,
                "agency_id": null,
                "name": "81/24 (81P)",
                "info_url": null,
                "wiki_url": "",
                "map_url": "http://maps.google.com/maps?q=46.071+N,+62.985+E",
                "latitude": "46.071001",
                "longitude": "62.984999",
                "location": {
                  "id": 15,
                  "name": "Baikonur Cosmodrome, Republic of Kazakhstan",
                  "country_code": "KAZ"
                }
              },
              "image_url": null,
              "infographic_url": null
            },
            {
              "id": "7690b919-288d-43aa-8e2f-f60b882b3f5a",
              "url": "https://spacelaunchnow.me/api/3.3.0/launch/7690b919-288d-43aa-8e2f-f60b882b3f5a/?format=json",
              "launch_library_id": 1075,
              "slug": "https://spacelaunchnow.me/launch/rokot-briz-km-gonets-m24-gonets-m25-gonets-m26-bli",
              "name": "Rokot / Briz-KM | Gonets-M24, Gonets-M25, Gonets-M26 & Blits-M",
              "img_url": null,
              "status": {
                "id": 2,
                "name": "TBD"
              },
              "net": "2019-12-26T23:12:00Z",
              "window_end": "2019-12-26T23:12:00Z",
              "window_start": "2019-12-26T23:12:00Z",
              "inhold": false,
              "tbdtime": true,
              "tbddate": true,
              "probability": -1,
              "holdreason": null,
              "failreason": null,
              "hashtag": null,
              "rocket": {
                "id": 4,
                "configuration": {
                  "id": 40,
                  "launch_library_id": 50,
                  "url": "https://spacelaunchnow.me/api/3.3.0/config/launcher/40/?format=json",
                  "name": "Rokot / Briz-KM",
                  "launch_service_provider": "Russian Aerospace Defence Forces"
                },
                "launcher_stage": [],
                "spacecraft_stage": null
              },
              "mission": {
                "id": 866,
                "launch_library_id": 330,
                "name": "Gonets-M24, Gonets-M25, Gonets-M26 & Blits-M",
                "description": "Gonets-M are an upgraded version of Gonets satellites, derived from military  communications system Strela-3. Gonets-M constellation is tasked with monitoring ecological and industrial objects, providing communication and data transmission services, covering also the remote areas like the Far North region. Gonets-M satellites share the ride with the Blits-M microsatellite, which is a geodesic satellite tasked with determining precise orbit parameters for GLONASS  satellites.",
                "type": "Communications",
                "orbit": "Low Earth Orbit",
                "orbit_abbrev": "LEO"
              },
              "pad": {
                "id": 3,
                "agency_id": 163,
                "name": "133/3 (133L)",
                "info_url": null,
                "wiki_url": "",
                "map_url": "http://maps.google.com/maps?q=62.887+N,+40.847+E",
                "latitude": "62.886999",
                "longitude": "40.846984",
                "location": {
                  "id": 6,
                  "name": "Plesetsk Cosmodrome, Russian Federation",
                  "country_code": "RUS"
                }
              },
              "image_url": null,
              "infographic_url": null
            },
            {
              "id": "6ebd8f81-5a6e-44da-8ac9-c8a3afc8645b",
              "url": "https://spacelaunchnow.me/api/3.3.0/launch/6ebd8f81-5a6e-44da-8ac9-c8a3afc8645b/?format=json",
              "launch_library_id": 1517,
              "slug": "https://spacelaunchnow.me/launch/long-march-5-shijian-20",
              "name": "Long March 5  | Shijian 20",
              "img_url": null,
              "status": {
                "id": 2,
                "name": "TBD"
              },
              "net": "2019-12-27T00:00:00Z",
              "window_end": "2019-12-27T00:00:00Z",
              "window_start": "2019-12-27T00:00:00Z",
              "inhold": false,
              "tbdtime": true,
              "tbddate": true,
              "probability": -1,
              "holdreason": null,
              "failreason": null,
              "hashtag": null,
              "rocket": {
                "id": 123,
                "configuration": {
                  "id": 128,
                  "launch_library_id": 116,
                  "url": "https://spacelaunchnow.me/api/3.3.0/config/launcher/128/?format=json",
                  "name": "Long March 5",
                  "launch_service_provider": "China Aerospace Science and Technology Corporation"
                },
                "launcher_stage": [],
                "spacecraft_stage": null
              },
              "mission": null,
              "pad": {
                "id": 78,
                "agency_id": null,
                "name": "Wenchang",
                "info_url": null,
                "wiki_url": "https://en.wikipedia.org/wiki/Wenchang_Satellite_Launch_Center",
                "map_url": "https://www.google.com/maps/place/19°36'52.2\"N+110°57'04.1\"E/",
                "latitude": "19.614354",
                "longitude": "110.951057",
                "location": {
                  "id": 8,
                  "name": "Wenchang Satellite Launch Center, People's Republic of China",
                  "country_code": "CHN"
                }
              },
              "image_url": null,
              "infographic_url": null
            },
            {
              "id": "6619844e-4777-4f7f-81d6-9ef0c3308d1d",
              "url": "https://spacelaunchnow.me/api/3.3.0/launch/6619844e-4777-4f7f-81d6-9ef0c3308d1d/?format=json",
              "launch_library_id": 1937,
              "slug": "https://spacelaunchnow.me/launch/kuaizhou-11-maiden-flight",
              "name": "Kuaizhou-11 | Maiden Flight",
              "img_url": null,
              "status": {
                "id": 2,
                "name": "TBD"
              },
              "net": "2019-12-31T00:00:00Z",
              "window_end": "2019-12-31T00:00:00Z",
              "window_start": "2019-12-31T00:00:00Z",
              "inhold": false,
              "tbdtime": true,
              "tbddate": true,
              "probability": -1,
              "holdreason": null,
              "failreason": null,
              "hashtag": null,
              "rocket": {
                "id": 2478,
                "configuration": {
                  "id": 194,
                  "launch_library_id": 223,
                  "url": "https://spacelaunchnow.me/api/3.3.0/config/launcher/194/?format=json",
                  "name": "Kuaizhou-11",
                  "launch_service_provider": "ExPace"
                },
                "launcher_stage": [],
                "spacecraft_stage": null
              },
              "mission": {
                "id": 1003,
                "launch_library_id": 1212,
                "name": "Maiden Flight",
                "description": "First flight of the new solid launcher developed by ExPace, subsidiary of CASIC. It will carry 2 communication satellites on this launch.",
                "type": "Test Flight",
                "orbit": "Low Earth Orbit",
                "orbit_abbrev": "LEO"
              },
              "pad": {
                "id": 71,
                "agency_id": null,
                "name": "Unknown Pad",
                "info_url": null,
                "wiki_url": "",
                "map_url": "",
                "latitude": "40.958",
                "longitude": "100.291",
                "location": {
                  "id": 17,
                  "name": "Jiuquan, People's Republic of China",
                  "country_code": "CHN"
                }
              },
              "image_url": null,
              "infographic_url": null
            },
            {
              "id": "a2851487-eb93-4a98-98a5-a130ed57937a",
              "url": "https://spacelaunchnow.me/api/3.3.0/launch/a2851487-eb93-4a98-98a5-a130ed57937a/?format=json",
              "launch_library_id": 1962,
              "slug": "https://spacelaunchnow.me/launch/falcon-9-block-5-starlink-2",
              "name": "Falcon 9 Block 5 | Starlink 2",
              "img_url": null,
              "status": {
                "id": 2,
                "name": "TBD"
              },
              "net": "2019-12-31T04:40:00Z",
              "window_end": "2019-12-31T04:40:00Z",
              "window_start": "2019-12-31T04:40:00Z",
              "inhold": false,
              "tbdtime": true,
              "tbddate": true,
              "probability": -1,
              "holdreason": null,
              "failreason": null,
              "hashtag": null,
              "rocket": {
                "id": 2517,
                "configuration": {
                  "id": 164,
                  "launch_library_id": 188,
                  "url": "https://spacelaunchnow.me/api/3.3.0/config/launcher/164/?format=json",
                  "name": "Falcon 9 Block 5",
                  "launch_service_provider": "SpaceX"
                },
                "launcher_stage": [],
                "spacecraft_stage": null
              },
              "mission": {
                "id": 1028,
                "launch_library_id": 1229,
                "name": "Starlink 2",
                "description": "A batch of 60 satellites for Starlink mega-constellation - SpaceX's project for space-based Internet communication system.",
                "type": "Communications",
                "orbit": "Low Earth Orbit",
                "orbit_abbrev": "LEO"
              },
              "pad": {
                "id": 80,
                "agency_id": 121,
                "name": "Space Launch Complex 40",
                "info_url": null,
                "wiki_url": "https://en.wikipedia.org/wiki/Cape_Canaveral_Air_Force_Station_Space_Launch_Complex_40",
                "map_url": "http://maps.google.com/maps?q=28.56194122,-80.57735736",
                "latitude": "28.56194122",
                "longitude": "-80.57735736",
                "location": {
                  "id": 12,
                  "name": "Cape Canaveral, FL, USA",
                  "country_code": "USA"
                }
              },
              "image_url": null,
              "infographic_url": null
            },
            {
              "id": "05c00251-c8db-4922-9244-d1e407427962",
              "url": "https://spacelaunchnow.me/api/3.3.0/launch/05c00251-c8db-4922-9244-d1e407427962/?format=json",
              "launch_library_id": 1660,
              "slug": "https://spacelaunchnow.me/launch/gslv-mk-ii-gsat-7c",
              "name": "GSLV Mk II | GSAT-7C",
              "img_url": null,
              "status": {
                "id": 2,
                "name": "TBD"
              },
              "net": "2020-01-01T00:00:00Z",
              "window_end": "2020-01-01T00:00:00Z",
              "window_start": "2020-01-01T00:00:00Z",
              "inhold": false,
              "tbdtime": true,
              "tbddate": true,
              "probability": -1,
              "holdreason": null,
              "failreason": null,
              "hashtag": null,
              "rocket": {
                "id": 2176,
                "configuration": {
                  "id": 168,
                  "launch_library_id": 60,
                  "url": "https://spacelaunchnow.me/api/3.3.0/config/launcher/168/?format=json",
                  "name": "GSLV Mk II",
                  "launch_service_provider": "Indian Space Research Organization"
                },
                "launcher_stage": [],
                "spacecraft_stage": null
              },
              "mission": null,
              "pad": {
                "id": 82,
                "agency_id": 31,
                "name": "Satish Dhawan Space Centre Second Launch Pad",
                "info_url": null,
                "wiki_url": "https://en.wikipedia.org/wiki/Satish_Dhawan_Space_Centre_Second_Launch_Pad",
                "map_url": "https://www.google.com/maps?q=13.7199,80.2304",
                "latitude": "13.7199",
                "longitude": "80.2304",
                "location": {
                  "id": 14,
                  "name": "Sriharikota, Republic of India",
                  "country_code": "IND"
                }
              },
              "image_url": null,
              "infographic_url": null
            },
            {
              "id": "0914dabd-ac92-444b-bf9f-7a6b08bb06e1",
              "url": "https://spacelaunchnow.me/api/3.3.0/launch/0914dabd-ac92-444b-bf9f-7a6b08bb06e1/?format=json",
              "launch_library_id": 1928,
              "slug": "https://spacelaunchnow.me/launch/launcherone-ignis-others",
              "name": "LauncherOne | Ignis & others",
              "img_url": null,
              "status": {
                "id": 2,
                "name": "TBD"
              },
              "net": "2020-01-01T00:00:00Z",
              "window_end": "2020-01-01T00:00:00Z",
              "window_start": "2020-01-01T00:00:00Z",
              "inhold": false,
              "tbdtime": true,
              "tbddate": true,
              "probability": -1,
              "holdreason": null,
              "failreason": null,
              "hashtag": null,
              "rocket": {
                "id": 2464,
                "configuration": {
                  "id": 141,
                  "launch_library_id": 173,
                  "url": "https://spacelaunchnow.me/api/3.3.0/config/launcher/141/?format=json",
                  "name": "LauncherOne",
                  "launch_service_provider": "Virgin Orbit"
                },
                "launcher_stage": [],
                "spacecraft_stage": null
              },
              "mission": null,
              "pad": {
                "id": 104,
                "agency_id": null,
                "name": "Guam International Airport",
                "info_url": null,
                "wiki_url": "https://en.wikipedia.org/wiki/Antonio_B._Won_Pat_International_Airport",
                "map_url": "https://www.google.com/maps/place/13°29'02.0\"N+144°47'50.0\"E/",
                "latitude": "13.483889",
                "longitude": "144.797222",
                "location": {
                  "id": 20,
                  "name": "Air launch to orbit",
                  "country_code": "UNK"
                }
              },
              "image_url": null,
              "infographic_url": null
            },
            {
              "id": "36c05136-e9c5-4d50-ae8e-8a2d38710071",
              "url": "https://spacelaunchnow.me/api/3.3.0/launch/36c05136-e9c5-4d50-ae8e-8a2d38710071/?format=json",
              "launch_library_id": 1576,
              "slug": "https://spacelaunchnow.me/launch/pslv-risat-2a",
              "name": "PSLV  | RISAT-2A",
              "img_url": null,
              "status": {
                "id": 2,
                "name": "TBD"
              },
              "net": "2020-01-01T00:00:00Z",
              "window_end": "2020-01-01T00:00:00Z",
              "window_start": "2020-01-01T00:00:00Z",
              "inhold": false,
              "tbdtime": true,
              "tbddate": true,
              "probability": -1,
              "holdreason": null,
              "failreason": null,
              "hashtag": null,
              "rocket": {
                "id": 144,
                "configuration": {
                  "id": 7,
                  "launch_library_id": 14,
                  "url": "https://spacelaunchnow.me/api/3.3.0/config/launcher/7/?format=json",
                  "name": "PSLV",
                  "launch_service_provider": "Indian Space Research Organization"
                },
                "launcher_stage": [],
                "spacecraft_stage": null
              },
              "mission": {
                "id": 572,
                "launch_library_id": 739,
                "name": "RISAT-2A",
                "description": "RISAT-2A is the third in the series of radar imaging RISAT satellites. The satellite carries a sophisticated synthetic aperture radar that operates at 5.35 GHz in C band.",
                "type": "Government/Top Secret",
                "orbit": "Low Earth Orbit",
                "orbit_abbrev": "LEO"
              },
              "pad": {
                "id": 50,
                "agency_id": 31,
                "name": "Satish Dhawan Space Centre First Launch Pad",
                "info_url": null,
                "wiki_url": "https://en.wikipedia.org/wiki/Satish_Dhawan_Space_Centre_First_Launch_Pad",
                "map_url": "https://www.google.com/maps?q=13.733,80.235",
                "latitude": "13.733",
                "longitude": "80.235",
                "location": {
                  "id": 14,
                  "name": "Sriharikota, Republic of India",
                  "country_code": "IND"
                }
              },
              "image_url": null,
              "infographic_url": null
            },
            {
              "id": "54542f3a-1a28-430b-a416-03f58e1a88d2",
              "url": "https://spacelaunchnow.me/api/3.3.0/launch/54542f3a-1a28-430b-a416-03f58e1a88d2/?format=json",
              "launch_library_id": 1357,
              "slug": "https://spacelaunchnow.me/launch/long-march-3b-fengyun-4b",
              "name": "Long March 3B | Fengyun-4B",
              "img_url": null,
              "status": {
                "id": 2,
                "name": "TBD"
              },
              "net": "2020-01-01T00:00:00Z",
              "window_end": "2020-01-01T00:00:00Z",
              "window_start": "2020-01-01T00:00:00Z",
              "inhold": false,
              "tbdtime": true,
              "tbddate": true,
              "probability": -1,
              "holdreason": null,
              "failreason": null,
              "hashtag": null,
              "rocket": {
                "id": 70,
                "configuration": {
                  "id": 19,
                  "launch_library_id": 22,
                  "url": "https://spacelaunchnow.me/api/3.3.0/config/launcher/19/?format=json",
                  "name": "Long March 3",
                  "launch_service_provider": "China Aerospace Science and Technology Corporation"
                },
                "launcher_stage": [],
                "spacecraft_stage": null
              },
              "mission": null,
              "pad": {
                "id": 86,
                "agency_id": 17,
                "name": "Unknown",
                "info_url": null,
                "wiki_url": "https://en.wikipedia.org/wiki/Xichang_Satellite_Launch_Center",
                "map_url": "https://www.google.com/maps?ll=28.246017,102.026556&q=28.246017,102.026556&hl=en&t=m&z=15",
                "latitude": "28.246017",
                "longitude": "102.026556",
                "location": {
                  "id": 16,
                  "name": "Xichang Satellite Launch Center, People's Republic of China",
                  "country_code": "CHN"
                }
              },
              "image_url": null,
              "infographic_url": null
            },
            {
              "id": "5c1c76b1-fe29-4b3f-97d0-c35695d5fbd0",
              "url": "https://spacelaunchnow.me/api/3.3.0/launch/5c1c76b1-fe29-4b3f-97d0-c35695d5fbd0/?format=json",
              "launch_library_id": 1292,
              "slug": "https://spacelaunchnow.me/launch/long-march-2f-shenzhou-12",
              "name": "Long March 2F | Shenzhou 12",
              "img_url": null,
              "status": {
                "id": 2,
                "name": "TBD"
              },
              "net": "2020-01-01T00:00:00Z",
              "window_end": "2020-01-01T00:00:00Z",
              "window_start": "2020-01-01T00:00:00Z",
              "inhold": false,
              "tbdtime": true,
              "tbddate": true,
              "probability": -1,
              "holdreason": null,
              "failreason": null,
              "hashtag": null,
              "rocket": {
                "id": 132,
                "configuration": {
                  "id": 79,
                  "launch_library_id": 92,
                  "url": "https://spacelaunchnow.me/api/3.3.0/config/launcher/79/?format=json",
                  "name": "Long March 2F",
                  "launch_service_provider": "China Aerospace Science and Technology Corporation"
                },
                "launcher_stage": [],
                "spacecraft_stage": null
              },
              "mission": null,
              "pad": {
                "id": 37,
                "agency_id": null,
                "name": "Launch Area 4 (SLS-1 / 921)",
                "info_url": null,
                "wiki_url": "https://en.wikipedia.org/wiki/Jiuquan_Launch_Area_4",
                "map_url": "http://maps.google.com/maps?q=40.958093,100.291188",
                "latitude": "40.958093",
                "longitude": "100.291188",
                "location": {
                  "id": 17,
                  "name": "Jiuquan, People's Republic of China",
                  "country_code": "CHN"
                }
              },
              "image_url": null,
              "infographic_url": null
            },
            {
              "id": "671b829d-ec26-4b9f-ac13-e50bdc9cca05",
              "url": "https://spacelaunchnow.me/api/3.3.0/launch/671b829d-ec26-4b9f-ac13-e50bdc9cca05/?format=json",
              "launch_library_id": 1880,
              "slug": "https://spacelaunchnow.me/launch/pslv-risat-1a",
              "name": "PSLV  | RISAT-1A",
              "img_url": null,
              "status": {
                "id": 2,
                "name": "TBD"
              },
              "net": "2020-01-01T00:00:00Z",
              "window_end": "2020-01-01T00:00:00Z",
              "window_start": "2020-01-01T00:00:00Z",
              "inhold": false,
              "tbdtime": true,
              "tbddate": true,
              "probability": -1,
              "holdreason": null,
              "failreason": null,
              "hashtag": null,
              "rocket": {
                "id": 2414,
                "configuration": {
                  "id": 7,
                  "launch_library_id": 14,
                  "url": "https://spacelaunchnow.me/api/3.3.0/config/launcher/7/?format=json",
                  "name": "PSLV",
                  "launch_service_provider": "Indian Space Research Organization"
                },
                "launcher_stage": [],
                "spacecraft_stage": null
              },
              "mission": null,
              "pad": {
                "id": 50,
                "agency_id": 31,
                "name": "Satish Dhawan Space Centre First Launch Pad",
                "info_url": null,
                "wiki_url": "https://en.wikipedia.org/wiki/Satish_Dhawan_Space_Centre_First_Launch_Pad",
                "map_url": "https://www.google.com/maps?q=13.733,80.235",
                "latitude": "13.733",
                "longitude": "80.235",
                "location": {
                  "id": 14,
                  "name": "Sriharikota, Republic of India",
                  "country_code": "IND"
                }
              },
              "image_url": null,
              "infographic_url": null
            },
            {
              "id": "6d847530-c328-445b-9689-6969ae689d79",
              "url": "https://spacelaunchnow.me/api/3.3.0/launch/6d847530-c328-445b-9689-6969ae689d79/?format=json",
              "launch_library_id": 1420,
              "slug": "https://spacelaunchnow.me/launch/long-march-4c-fengyun-3e",
              "name": "Long March 4C | Fengyun-3E",
              "img_url": null,
              "status": {
                "id": 2,
                "name": "TBD"
              },
              "net": "2020-01-01T00:00:00Z",
              "window_end": "2020-01-01T00:00:00Z",
              "window_start": "2020-01-01T00:00:00Z",
              "inhold": false,
              "tbdtime": true,
              "tbddate": true,
              "probability": -1,
              "holdreason": null,
              "failreason": null,
              "hashtag": null,
              "rocket": {
                "id": 73,
                "configuration": {
                  "id": 64,
                  "launch_library_id": 70,
                  "url": "https://spacelaunchnow.me/api/3.3.0/config/launcher/64/?format=json",
                  "name": "Long March 4",
                  "launch_service_provider": "China Aerospace Science and Technology Corporation"
                },
                "launcher_stage": [],
                "spacecraft_stage": null
              },
              "mission": null,
              "pad": {
                "id": 60,
                "agency_id": null,
                "name": "Unknown Pad",
                "info_url": null,
                "wiki_url": "",
                "map_url": "",
                "latitude": "38.849",
                "longitude": "111.608",
                "location": {
                  "id": 19,
                  "name": "Taiyuan, People's Republic of China",
                  "country_code": "CHN"
                }
              },
              "image_url": null,
              "infographic_url": null
            },
            {
              "id": "6ec66610-b96c-4e3d-86f2-207068789781",
              "url": "https://spacelaunchnow.me/api/3.3.0/launch/6ec66610-b96c-4e3d-86f2-207068789781/?format=json",
              "launch_library_id": 1824,
              "slug": "https://spacelaunchnow.me/launch/pslv-oceansat-3",
              "name": "PSLV  | Oceansat-3",
              "img_url": null,
              "status": {
                "id": 2,
                "name": "TBD"
              },
              "net": "2020-01-01T00:00:00Z",
              "window_end": "2020-01-01T00:00:00Z",
              "window_start": "2020-01-01T00:00:00Z",
              "inhold": false,
              "tbdtime": true,
              "tbddate": true,
              "probability": -1,
              "holdreason": null,
              "failreason": null,
              "hashtag": null,
              "rocket": {
                "id": 2356,
                "configuration": {
                  "id": 7,
                  "launch_library_id": 14,
                  "url": "https://spacelaunchnow.me/api/3.3.0/config/launcher/7/?format=json",
                  "name": "PSLV",
                  "launch_service_provider": "Indian Space Research Organization"
                },
                "launcher_stage": [],
                "spacecraft_stage": null
              },
              "mission": {
                "id": 981,
                "launch_library_id": 1193,
                "name": "Oceansat-3",
                "description": "Oceansat-3 is a part of ISRO's Oceansat program. Its main purpose is ocean observation, which includes gathering ocean color data, sea surface temperature measurements and wind vector data.",
                "type": "Earth Science",
                "orbit": "Sun-Synchronous Orbit",
                "orbit_abbrev": "SSO"
              },
              "pad": {
                "id": 50,
                "agency_id": 31,
                "name": "Satish Dhawan Space Centre First Launch Pad",
                "info_url": null,
                "wiki_url": "https://en.wikipedia.org/wiki/Satish_Dhawan_Space_Centre_First_Launch_Pad",
                "map_url": "https://www.google.com/maps?q=13.733,80.235",
                "latitude": "13.733",
                "longitude": "80.235",
                "location": {
                  "id": 14,
                  "name": "Sriharikota, Republic of India",
                  "country_code": "IND"
                }
              },
              "image_url": null,
              "infographic_url": null
            },
            {
              "id": "73e676bb-3f80-4909-b3d7-026f9f1ee5d8",
              "url": "https://spacelaunchnow.me/api/3.3.0/launch/73e676bb-3f80-4909-b3d7-026f9f1ee5d8/?format=json",
              "launch_library_id": 1197,
              "slug": "https://spacelaunchnow.me/launch/long-march-3be-apstar-6d",
              "name": "Long March 3B/E | APStar-6D",
              "img_url": null,
              "status": {
                "id": 2,
                "name": "TBD"
              },
              "net": "2020-01-01T00:00:00Z",
              "window_end": "2020-01-01T00:00:00Z",
              "window_start": "2020-01-01T00:00:00Z",
              "inhold": false,
              "tbdtime": true,
              "tbddate": true,
              "probability": -1,
              "holdreason": null,
              "failreason": null,
              "hashtag": null,
              "rocket": {
                "id": 58,
                "configuration": {
                  "id": 50,
                  "launch_library_id": 69,
                  "url": "https://spacelaunchnow.me/api/3.3.0/config/launcher/50/?format=json",
                  "name": "Long March 3",
                  "launch_service_provider": "China Aerospace Science and Technology Corporation"
                },
                "launcher_stage": [],
                "spacecraft_stage": null
              },
              "mission": {
                "id": 528,
                "launch_library_id": 679,
                "name": "APStar-6D",
                "description": "APStar-6D is a commercial communications satellite for APT Satellite Company Ltd under an end-to-end contract signed with China Great Wall Industry Corp. (CGWIC) for satellite production and launch services. It will deliver VSAT, video distribution, Direct-to-home television and cellular backhaul to the Asia-Pacific Region.",
                "type": "Communications",
                "orbit": "Geostationary Transfer Orbit",
                "orbit_abbrev": "GTO"
              },
              "pad": {
                "id": 45,
                "agency_id": null,
                "name": "Launch Complex 2 (LC-2)",
                "info_url": null,
                "wiki_url": "https://en.wikipedia.org/wiki/Xichang_Satellite_Launch_Center",
                "map_url": "https://www.google.com/maps/?q=28.246017,102.026556",
                "latitude": "28.246017",
                "longitude": "102.026556",
                "location": {
                  "id": 16,
                  "name": "Xichang Satellite Launch Center, People's Republic of China",
                  "country_code": "CHN"
                }
              },
              "image_url": null,
              "infographic_url": null
            },
            {
              "id": "7fc8b29d-a9ab-4269-838d-7d3b0fc8f26a",
              "url": "https://spacelaunchnow.me/api/3.3.0/launch/7fc8b29d-a9ab-4269-838d-7d3b0fc8f26a/?format=json",
              "launch_library_id": 1250,
              "slug": "https://spacelaunchnow.me/launch/zenit-3slb-lybid",
              "name": "Zenit 3SLB | Lybid",
              "img_url": null,
              "status": {
                "id": 2,
                "name": "TBD"
              },
              "net": "2020-01-01T00:00:00Z",
              "window_end": "2020-01-01T00:00:00Z",
              "window_start": "2020-01-01T00:00:00Z",
              "inhold": false,
              "tbdtime": true,
              "tbddate": true,
              "probability": -1,
              "holdreason": null,
              "failreason": null,
              "hashtag": null,
              "rocket": {
                "id": 113,
                "configuration": {
                  "id": 91,
                  "launch_library_id": 94,
                  "url": "https://spacelaunchnow.me/api/3.3.0/config/launcher/91/?format=json",
                  "name": "Zenit",
                  "launch_service_provider": "Land Launch"
                },
                "launcher_stage": [],
                "spacecraft_stage": null
              },
              "mission": {
                "id": 581,
                "launch_library_id": 750,
                "name": "Lybid",
                "description": "Lybid is a geostationary telecommunications satellite for Ukraine. Lybid is planned be located at 48 degrees East and have a service lifetime of 15 years.",
                "type": "Communications",
                "orbit": "Geostationary Transfer Orbit",
                "orbit_abbrev": "GTO"
              },
              "pad": {
                "id": 2,
                "agency_id": null,
                "name": "45/1",
                "info_url": null,
                "wiki_url": "",
                "map_url": "http://maps.google.com/maps?q=45.9435+N,+63.653+E",
                "latitude": "45.943492",
                "longitude": "63.653014",
                "location": {
                  "id": 15,
                  "name": "Baikonur Cosmodrome, Republic of Kazakhstan",
                  "country_code": "KAZ"
                }
              },
              "image_url": null,
              "infographic_url": null
            },
            {
              "id": "c9fb70ab-85b1-40dd-b98e-67b37efc7e94",
              "url": "https://spacelaunchnow.me/api/3.3.0/launch/c9fb70ab-85b1-40dd-b98e-67b37efc7e94/?format=json",
              "launch_library_id": 1291,
              "slug": "https://spacelaunchnow.me/launch/long-march-7-tianzhou-2",
              "name": "Long March 7  | Tianzhou-2",
              "img_url": null,
              "status": {
                "id": 2,
                "name": "TBD"
              },
              "net": "2020-01-01T00:00:00Z",
              "window_end": "2020-01-01T00:00:00Z",
              "window_start": "2020-01-01T00:00:00Z",
              "inhold": false,
              "tbdtime": true,
              "tbddate": true,
              "probability": -1,
              "holdreason": null,
              "failreason": null,
              "hashtag": null,
              "rocket": {
                "id": 131,
                "configuration": {
                  "id": 100,
                  "launch_library_id": 130,
                  "url": "https://spacelaunchnow.me/api/3.3.0/config/launcher/100/?format=json",
                  "name": "Long March 7",
                  "launch_service_provider": "China Aerospace Science and Technology Corporation"
                },
                "launcher_stage": [],
                "spacecraft_stage": null
              },
              "mission": null,
              "pad": {
                "id": 78,
                "agency_id": null,
                "name": "Wenchang",
                "info_url": null,
                "wiki_url": "https://en.wikipedia.org/wiki/Wenchang_Satellite_Launch_Center",
                "map_url": "https://www.google.com/maps/place/19°36'52.2\"N+110°57'04.1\"E/",
                "latitude": "19.614354",
                "longitude": "110.951057",
                "location": {
                  "id": 8,
                  "name": "Wenchang Satellite Launch Center, People's Republic of China",
                  "country_code": "CHN"
                }
              },
              "image_url": null,
              "infographic_url": null
            },
            {
              "id": "cb0c27fd-496c-4f74-aa04-9c0e5af33e3c",
              "url": "https://spacelaunchnow.me/api/3.3.0/launch/cb0c27fd-496c-4f74-aa04-9c0e5af33e3c/?format=json",
              "launch_library_id": 1308,
              "slug": "https://spacelaunchnow.me/launch/epsilon-jv-lotusat-1",
              "name": "Epsilon  | JV-LOTUSat-1",
              "img_url": null,
              "status": {
                "id": 2,
                "name": "TBD"
              },
              "net": "2020-01-01T00:00:00Z",
              "window_end": "2020-01-01T00:00:00Z",
              "window_start": "2020-01-01T00:00:00Z",
              "inhold": false,
              "tbdtime": true,
              "tbddate": true,
              "probability": -1,
              "holdreason": null,
              "failreason": null,
              "hashtag": null,
              "rocket": {
                "id": 63,
                "configuration": {
                  "id": 112,
                  "launch_library_id": 168,
                  "url": "https://spacelaunchnow.me/api/3.3.0/config/launcher/112/?format=json",
                  "name": "Epsilon",
                  "launch_service_provider": "Japan Aerospace Exploration Agency"
                },
                "launcher_stage": [],
                "spacecraft_stage": null
              },
              "mission": null,
              "pad": {
                "id": 47,
                "agency_id": 37,
                "name": "Mu Center",
                "info_url": null,
                "wiki_url": "https://en.wikipedia.org/wiki/Uchinoura_Space_Center",
                "map_url": "https://www.google.com/maps?q=31.251,131.0813",
                "latitude": "31.251",
                "longitude": "131.0813",
                "location": {
                  "id": 24,
                  "name": "Uchinoura Space Center, Japan",
                  "country_code": "JPN"
                }
              },
              "image_url": null,
              "infographic_url": null
            }
          ]
        }
        """

        return gson.fromJson(json, SpaceLaunch::class.java)
    }
}

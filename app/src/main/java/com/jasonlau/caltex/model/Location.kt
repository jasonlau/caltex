package com.jasonlau.caltex.model

import com.google.gson.annotations.SerializedName

data class Location(
    val name: String?,
    @SerializedName("country_code") val countryCode: String?
)

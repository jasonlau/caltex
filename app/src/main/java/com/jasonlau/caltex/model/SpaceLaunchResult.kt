package com.jasonlau.caltex.model

import com.google.gson.annotations.SerializedName

data class SpaceLaunchResult(
    val status: Status?,
    val pad: Pad?,
    val rocket: Rocket?,
    @SerializedName("window_start") val windowStart: String?,
    @SerializedName("image_url") val imageUrl: String?
)

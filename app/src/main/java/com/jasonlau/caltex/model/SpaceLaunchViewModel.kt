package com.jasonlau.caltex.model

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jasonlau.caltex.DispatcherProvider
import com.jasonlau.caltex.service.ServiceResult
import com.jasonlau.caltex.service.SingleLiveEvent
import com.jasonlau.caltex.service.SpaceLaunchRepository
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class SpaceLaunchViewModel(
    val spaceLaunchRepository: SpaceLaunchRepository,
    val dispatcherProvider: DispatcherProvider
) : ViewModel(), CoroutineScope {
    private val job = Job()
    override val coroutineContext: CoroutineContext = dispatcherProvider.main() + job

    val showLoading = MutableLiveData<Boolean>()
    val spaceLaunch = MutableLiveData<SpaceLaunch>()
    val showError = SingleLiveEvent<String>()

    fun getSpaceLaunches() {
        launch {
            showLoading.value = true
            val result = withContext(dispatcherProvider.io()) { spaceLaunchRepository.getSpaceLaunches() }
            showLoading.value = false
            when (result) {
                is ServiceResult.Success -> spaceLaunch.value = result.data
                is ServiceResult.Error -> showError.value = result.exception.message
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }
}

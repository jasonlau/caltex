package com.jasonlau.caltex.model

data class SpaceLaunch(val results: List<SpaceLaunchResult>?)

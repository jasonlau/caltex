package com.jasonlau.caltex.model

import com.google.gson.annotations.SerializedName

data class RocketDetails(
    @SerializedName("image_url") val imageUrl: String?,
    val description: String?
)

package com.jasonlau.caltex.model

data class Configuration(
    val name: String?,
    val url: String?
)

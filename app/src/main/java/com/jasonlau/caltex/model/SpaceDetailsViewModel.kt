package com.jasonlau.caltex.model

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jasonlau.caltex.DispatcherProvider
import com.jasonlau.caltex.service.ServiceResult
import com.jasonlau.caltex.service.SingleLiveEvent
import com.jasonlau.caltex.service.SpaceLaunchRepository
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class SpaceDetailsViewModel(
    val spaceLaunchRepository: SpaceLaunchRepository,
    val dispatcherProvider: DispatcherProvider
) : ViewModel(), CoroutineScope {
    private val job = Job()
    override val coroutineContext: CoroutineContext = dispatcherProvider.main() + job

    val rocketDetails = MutableLiveData<RocketDetails?>()
    val showError = SingleLiveEvent<String>()

    fun getRocketDetails(url: String?) {
        launch {
            val result = withContext(dispatcherProvider.io()) {
                url?.let {
                    spaceLaunchRepository.getRocketDetails(it)
                } ?: ServiceResult.Error(Throwable("Cannot find rocket image path"))
            }
            when (result) {
                is ServiceResult.Success -> rocketDetails.value = result.data
                is ServiceResult.Error -> showError.value = result.exception.message
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }
}

package com.jasonlau.caltex.service

import com.jasonlau.caltex.model.RocketDetails
import com.jasonlau.caltex.model.SpaceLaunch
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Url

interface SpaceLaunchService {
    @GET("api/3.3.0/launch/upcoming?format=json")
    fun getSpaceLaunches() : Deferred<SpaceLaunch>

    @GET
    fun getRocketDetails(@Url url: String) : Deferred<RocketDetails>
}

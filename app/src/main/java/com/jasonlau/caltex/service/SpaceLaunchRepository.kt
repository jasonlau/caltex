package com.jasonlau.caltex.service

import com.jasonlau.caltex.model.RocketDetails
import com.jasonlau.caltex.model.SpaceLaunch

interface SpaceLaunchRepository {
    suspend fun getSpaceLaunches() : ServiceResult<SpaceLaunch>
    suspend fun getRocketDetails(url: String) : ServiceResult<RocketDetails>
}

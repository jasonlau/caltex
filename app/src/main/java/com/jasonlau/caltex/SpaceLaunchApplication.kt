package com.jasonlau.caltex

import android.app.Application
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.jasonlau.caltex.model.SpaceDetailsViewModel
import com.jasonlau.caltex.model.SpaceLaunchViewModel
import com.jasonlau.caltex.service.SpaceLaunchRepository
import com.jasonlau.caltex.service.SpaceLaunchRepositoryImpl
import com.jasonlau.caltex.service.SpaceLaunchService
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

private const val BASE_URL = "https://spacelaunchnow.me/"

class SpaceLaunchApplication : Application() {
    private val appModule = module {
        single<SpaceLaunchService> {
            Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()
                .create(SpaceLaunchService::class.java)
        }

        factory<SpaceLaunchRepository> {
            SpaceLaunchRepositoryImpl(spaceLaunchService = get())
        }

        factory<DispatcherProvider> {
            DefaultDispatcherProvider()
        }

        viewModel {
            SpaceLaunchViewModel(spaceLaunchRepository = get(), dispatcherProvider = get())
        }

        viewModel {
            SpaceDetailsViewModel(spaceLaunchRepository = get(), dispatcherProvider = get())
        }
    }

    override fun onCreate() {
        super.onCreate()

        startKoin{
            androidLogger()
            androidContext(this@SpaceLaunchApplication)
            modules(appModule)
        }
    }
}

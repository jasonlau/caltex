package com.jasonlau.caltex.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.jasonlau.caltex.R
import com.jasonlau.caltex.model.SpaceLaunchResult
import com.jasonlau.caltex.model.SpaceLaunchViewModel
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel

import org.koin.core.parameter.parametersOf

const val EXTRA_ROCKET_NAME = "EXTRA_ROCKET_NAME"
const val EXTRA_ROCKET_DETAILS_URL = "EXTRA_ROCKET_DETAILS_URL"
const val EXTRA_LOCATION = "EXTRA_LOCATION"
const val EXTRA_STATUS = "EXTRA_STATUS"
const val EXTRA_DATE = "EXTRA_DATE"

class MainActivity : AppCompatActivity() {
    private val viewModel: SpaceLaunchViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar as Toolbar)
        initViewModel()
    }

    private fun showResults(results: List<SpaceLaunchResult?>?) {
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = SpaceLaunchAdapter(results) {
            val intent = Intent(this, SpaceDetailsActivity::class.java)
            intent.putExtra(EXTRA_ROCKET_NAME, it.rocket?.configuration?.name)
            intent.putExtra(EXTRA_ROCKET_DETAILS_URL, it.rocket?.configuration?.url)
            intent.putExtra(EXTRA_LOCATION, it.pad?.location?.name)
            intent.putExtra(EXTRA_STATUS, it.status?.name)
            intent.putExtra(EXTRA_DATE, it.windowStart)
            startActivity(intent)
        }
        recyclerView.addItemDecoration(DividerItemDecoration(recyclerView.context, DividerItemDecoration.VERTICAL))
    }

    private fun showError() {
        Toast.makeText(this, "unable to show results", Toast.LENGTH_LONG).show()
    }

    private fun initViewModel() {
        viewModel.spaceLaunch.observe(this, Observer { spaceLaunch ->
            showResults(spaceLaunch.results)
        })

        viewModel.showLoading.observe(this, Observer { showLoading ->
            mainProgressBar.visibility = if (showLoading) View.VISIBLE else View.GONE
        })

        viewModel.showError.observe(this, Observer {
            showError()
        })

        viewModel.getSpaceLaunches()
    }
}

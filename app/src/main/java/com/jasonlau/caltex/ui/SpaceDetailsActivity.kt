package com.jasonlau.caltex.ui

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import com.jasonlau.caltex.R
import com.jasonlau.caltex.model.SpaceDetailsViewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_space_details.*
import org.koin.android.viewmodel.ext.android.viewModel
import java.text.SimpleDateFormat
import java.util.*

class SpaceDetailsActivity: AppCompatActivity() {
    private val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault())
    private val displayFormat = SimpleDateFormat("dd MMM yyyy h:mm aa", Locale.ENGLISH)
    private val viewModel: SpaceDetailsViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_space_details)
        val rocketName = intent.getStringExtra(EXTRA_ROCKET_NAME)
        val rocketDetailsUrl = intent.getStringExtra(EXTRA_ROCKET_DETAILS_URL)
        val rocketLocation = intent.getStringExtra(EXTRA_LOCATION)
        val rocketStatus = intent.getStringExtra(EXTRA_STATUS)
        val launchDate = intent.getStringExtra(EXTRA_DATE)
        name.text = rocketName
        location.text = rocketLocation
        status.text = rocketStatus
        date.text = parseDate(launchDate)

        setSupportActionBar(toolbar as Toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back)

        initViewModel(rocketDetailsUrl)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun initViewModel(rocketDetailsUrl: String?) {
        viewModel.rocketDetails.observe(this, Observer { details ->
            showRocketImage(details?.imageUrl)
            if (details?.description?.isNotEmpty() == true) {
                description_label.visibility = View.VISIBLE
                description.visibility = View.VISIBLE
                description.text = details.description
            }
        })

        viewModel.showError.observe(this, Observer {
            showError()
            description_label.visibility = View.GONE
            description.visibility = View.GONE
        })

        viewModel.getRocketDetails(rocketDetailsUrl)
    }

    private fun showError() {
        Toast.makeText(this, "unable to retrieve rocket details", Toast.LENGTH_LONG).show()
    }

    private fun parseDate(windowStart: String?): String? {
        return windowStart?.let {
            val parse = dateFormat.parse(it)
            parse?.let {
                displayFormat.format(it)
            }
        }
    }

    private fun showRocketImage(imageUrl: String?) {
        imageUrl?.let {
            Picasso.with(this)
                .load(it)
                .into(image)
        }
    }
}

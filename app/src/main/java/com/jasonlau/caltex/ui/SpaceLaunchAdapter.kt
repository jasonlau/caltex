package com.jasonlau.caltex.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jasonlau.caltex.R
import com.jasonlau.caltex.model.SpaceLaunchResult
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.list_item.view.*

class SpaceLaunchAdapter(private val spaceLaunches: List<SpaceLaunchResult?>?, private val onItemClicked: (SpaceLaunchResult) -> Unit)
    : RecyclerView.Adapter<SpaceLaunchAdapter.SpaceLaunchViewHolder>() {
    private val particularInterestCountryCodes = listOf("RUS", "CHN", "UNK")

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SpaceLaunchViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false)
        return SpaceLaunchViewHolder(view)
    }

    override fun getItemCount(): Int = spaceLaunches?.size ?: 0

    override fun onBindViewHolder(holder: SpaceLaunchViewHolder, position: Int) {
        holder.bind(spaceLaunches?.get(position))
    }

    private fun isOfParticularInterestCountryCode(countryCode: String): Boolean {
        return particularInterestCountryCodes.contains(countryCode)
    }

    inner class SpaceLaunchViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {

        fun bind(spaceLaunchResult: SpaceLaunchResult?) {
            view.rocketName.text = spaceLaunchResult?.rocket?.configuration?.name
            view.location.text = spaceLaunchResult?.pad?.location?.name
            spaceLaunchResult?.pad?.location?.countryCode?.let {
                if (isOfParticularInterestCountryCode(it)) {
                    view.warning.visibility = View.VISIBLE
                } else {
                    view.warning.visibility = View.GONE
                }
            }

            if (spaceLaunchResult?.imageUrl?.isNotBlank() == true) {
                view.thumbnail.visibility = View.VISIBLE
                Picasso.with(view.context).load(spaceLaunchResult.imageUrl).into(view.thumbnail)
            } else {
                view.thumbnail.visibility = View.GONE
            }

            view.setOnClickListener {
                spaceLaunchResult?.let { launchResult -> onItemClicked.invoke(launchResult) }
            }
        }
    }
}

package com.jasonlau.caltex

import android.view.View
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.BoundedMatcher
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.jasonlau.caltex.ui.MainActivity
import com.jasonlau.caltex.ui.SpaceLaunchAdapter
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class SpaceLaunchFunctionalTest {
    @get:Rule
    val activityRule = ActivityTestRule(MainActivity::class.java)

    @Test fun shouldShowSpaceLaunchDetails() {
        onView(withId(R.id.recyclerView))
            .check(matches(atPosition(2, hasDescendant(withText("Atlas V N22")))))

        onView(withId(R.id.recyclerView))
            .perform(RecyclerViewActions.actionOnItemAtPosition<SpaceLaunchAdapter.SpaceLaunchViewHolder>(2, click()));


        onView(withId(R.id.name)).check(matches(withText("Atlas V N22")))
        onView(withId(R.id.location)).check(matches(withText("Cape Canaveral, FL, USA")))
        onView(withId(R.id.status)).check(matches(withText("Go")))
        onView(withId(R.id.date)).check(matches(withText("20-Dec-2019 11:36 AM")))
    }

    private fun atPosition(position: Int, @NonNull itemMatcher: Matcher<View>): Matcher<View> {
        return object : BoundedMatcher<View, RecyclerView>(RecyclerView::class.java) {
            override fun describeTo(description: Description) {
                description.appendText("has item at position $position: ")
                itemMatcher.describeTo(description)
            }

            override fun matchesSafely(view: RecyclerView): Boolean {
                val viewHolder = view.findViewHolderForAdapterPosition(position)
                    ?: // has no item on such position
                    return false
                return itemMatcher.matches(viewHolder.itemView)
            }
        }
    }
}

package com.jasonlau.caltex.service

import com.jasonlau.caltex.model.RocketDetails
import com.jasonlau.caltex.model.SpaceLaunch

class SpaceLaunchRepositoryImpl(private val spaceLaunchService: SpaceLaunchService) : SpaceLaunchRepository {
    override suspend fun getRocketDetails(url: String): ServiceResult<RocketDetails> {
        return try {
            val result = spaceLaunchService.getRocketDetails(url).await()
            ServiceResult.Success(result)
        } catch (ex: Exception) {
            ServiceResult.Error(ex)
        }
    }

    override suspend fun getSpaceLaunches(): ServiceResult<SpaceLaunch> {
        return try {
            val result = spaceLaunchService.getSpaceLaunches().await()
            ServiceResult.Success(result)
        } catch (ex: Exception) {
            ServiceResult.Error(ex)
        }
    }
}
